#include <boost/filesystem.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/console/parse.h>
#include <chrono>
#include "FRIClient.h" //This has to be included before windows.h

//OS Specific Libraries
#include <windows.h>

static const int unusedSandbox = 0;
//Local Libraries
#include "visualizer.h"
#include "fileIO.h"
#include "iiwaRobot.h"


void obs_createConfig();

typedef std::chrono::high_resolution_clock Clock;

FRIClient *friClient;

std::thread uiThread;
std::thread robotControlThread;
boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizer;

bool uiKillFlag = false;
bool robotKillFlag = false;
bool anglesEstablished = false;

long aveRobTime = 0;
long maxRobTime = 0;

Robot nullPosIIWA;
Robot tempLastRobot;

double targetSpeedMMpS = 50;

double targetLeadDistMM = 4;//2
double targetBlendRadiusMM = 2;//1

//double DemoTranslationStep = 1.5 / 50;
//double DemoTotalTranslation = 0;
double DemoTranslationCenter = 0;
double DemoTranslationTheta = 0;
double DemoTranslationFrequency = 0.15;//Hz
double DemoTranslationAmplitude = 25; //MM

double DemoRotationStep = (M_PI / 400) / 100;
double DemoTotalRotation = 0;

double robotKinematicsWarmup = 0;
double warmupStep = 0.0005;

std::mutex pathMutex;
RobotPath debugPath;
Eigen::Matrix4f DemoPathHandle = Eigen::Matrix4f::Identity();
Eigen::Matrix4f commandFrame = Eigen::Matrix4f::Identity();

std::mutex jointAngleMutex;


std::vector<double> lastJointAngles = std::vector<double>();
std::vector<double> commandJointAngles = std::vector<double>();


bool debugMode = false; //Allows system to be run without being connected to robot

void uiDoWork()
{
	visualizer = RoboVis::spawnRoboViewer();
	while (!uiKillFlag && !visualizer->wasStopped())
	{
		if (anglesEstablished) {
			if (pathMutex.try_lock())
			{
				std::vector<Eigen::Matrix4f> curPath = debugPath.getTotalPath();
				Eigen::Matrix4f tempHandle = DemoPathHandle;
				pathMutex.unlock();
				RoboVis::drawRobotPath(visualizer, curPath);
				RoboVis::drawRefFrame(tempHandle, "PathHandle", visualizer);
				RoboVis::drawRefFrame(commandFrame, "PathHandle", visualizer, 1);
			}

			if (jointAngleMutex.try_lock())
			{
				Robot iiwa = nullPosIIWA;
				iiwa.gotoAngles(lastJointAngles);
				jointAngleMutex.unlock();
				//RoboVis::drawRobotFrames(visualizer, iiwa);
				RoboVis::drawRefFrame(iiwa.getJointFrame(7), "iiwaFrame", visualizer, 0.5, 20);
				RoboVis::drawRobotShell(visualizer, iiwa, false);
			}
		}
		visualizer->spinOnce(50);
	}
}

void robotDoWork()
{
	//initializing angles based on mode.
	if (debugMode) { lastJointAngles = tempLastRobot.getJointAngles(); }
	else{
		friClient = new FRIClient();
		friClient->connect();
		while (!friClient->isFRIInActiveMode()) {
			boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
			std::cout << "Not in command mode" << std::endl;
		}
		std::cout << "robot connected" << std::endl;
		lastJointAngles = friClient->getMeasuredJointAngs();
	}
	//setting the anglesEstablished flag to true
	anglesEstablished = true;

	int serialPeriodUS = 1000;
	if (debugMode) { serialPeriodUS = 10000; }

	auto t1 = Clock::now();
	debugPath.resetSpeedClock();
	while (!robotKillFlag)
	{
		//This is a hard loop and locks a processor to get as reliable timing on this loop as possible...
		long curTime = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now() - t1).count();

		if (curTime > serialPeriodUS)
		{
			aveRobTime = aveRobTime * 0.9 + curTime * 0.1;
			if (curTime > maxRobTime) { maxRobTime = curTime; }
			t1 = Clock::now();

			//get updated robot angles
			Robot updatedRobot = nullPosIIWA;
			jointAngleMutex.lock();//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			if(debugMode){ lastJointAngles = tempLastRobot.getJointAngles(); }
			else{ lastJointAngles = friClient->getMeasuredJointAngs(); }

			jointAngleMutex.unlock();//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			updatedRobot.gotoAngles(lastJointAngles);
			

			//get updated sensor data and update path
			{//TODO: this is temporary for demonstration and should be replaced with actual sensor data
				/*if (abs(DemoTotalTranslation) > 100)
				{
					DemoTranslationStep = -DemoTranslationStep;
				}
				DemoTotalTranslation += DemoTranslationStep;*/
				double DemoTranslationStep = DemoTranslationAmplitude * sin(DemoTranslationTheta);
				DemoTranslationTheta += (2*M_PI) * ( DemoTranslationFrequency/1000.0);
				if (abs(DemoTranslationTheta) > 2 * M_PI) { DemoTranslationTheta - 2 * M_PI; }

				if (abs(DemoTotalRotation) > M_PI / 8)
				{
					DemoRotationStep = -DemoRotationStep;
				}
				DemoTotalRotation += DemoRotationStep;
				

				
				
				pathMutex.lock();//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
				DemoPathHandle(2, 3) = DemoTranslationCenter + DemoTranslationStep;
				//RobotMath::rotateAboutAxis(DemoPathHandle, DemoPathHandle.col(3).head<3>(), DemoPathHandle.col(0).head<3>(), DemoRotationStep);
				debugPath.updatePathHandle(DemoPathHandle);
				pathMutex.unlock();//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			}

			//calculate path
			bool pathComplete = false;
			pathMutex.lock();//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			commandFrame = updatedRobot.getJointFrame(updatedRobot.getJointCount() - 1);
			if(debugMode || friClient->isFRIInActiveMode()){ commandFrame = debugPath.simpleSpeedPathing(updatedRobot.getJointFrame(updatedRobot.getJointCount() - 1), targetSpeedMMpS, pathComplete, true); }
			else { debugPath.resetSpeedClock(); }

			pathMutex.unlock();//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

			//calculate IK
			std::vector<double> tempCommandAngles = iiwaKinematics::sphereKineToFrame(commandFrame, updatedRobot);
			
			jointAngleMutex.lock();//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			commandJointAngles.clear();

			if(debugMode || friClient->isFRIInActiveMode())
			{
				double speedMult = 1.0;
				if (robotKinematicsWarmup < 1)
				{
					robotKinematicsWarmup += warmupStep;
					speedMult = robotKinematicsWarmup;
				}

				for (int i = 0; i < 7; i++)
				{
					commandJointAngles.push_back(tempCommandAngles[i] * speedMult + lastJointAngles[i] * (1.0-speedMult));
				}
			}
			else
			{
				robotKinematicsWarmup = 0;
				for (int i = 0; i < 7; i++)
				{
					commandJointAngles.push_back(lastJointAngles[i]);
				}
			}

			//send drive angles to robot
			if (!debugMode) 
			{ 
				friClient->updateAngles(commandJointAngles);
				tempLastRobot.gotoAngles(lastJointAngles);
			}
			else
			{
				tempLastRobot.gotoAngles(commandJointAngles);
			}
			jointAngleMutex.unlock();//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

		}
	}
}

void initializeDemo()
{
	nullPosIIWA = fileIO::loadRobotPose();
	iiwaKinematics::initializeKinematics(nullPosIIWA);

	tempLastRobot = nullPosIIWA;
	tempLastRobot.gotoAngle(1, M_PI / 4);
	tempLastRobot.gotoAngle(5, M_PI / 4);

	Eigen::Matrix4f robTarget = nullPosIIWA.getJointFrame(0);

	RobotMath::rotateAboutAxis(robTarget, robTarget.col(3).head<3>(), robTarget.col(1).head<3>(), M_PI);

	std::vector<pcl::PointXYZ> testPath = std::vector<pcl::PointXYZ>();
	for (double i = 0; i < 2 * M_PI; i += M_PI / 150)//32
	{
		testPath.push_back(pcl::PointXYZ(100 * cos(i), 100 * sin(i), 0));
	}
	debugPath = RobotPath(testPath, robTarget);

	DemoPathHandle = Eigen::Matrix4f::Identity();
	DemoPathHandle(0, 3) = 600;
	DemoPathHandle(2, 3) = 300;
	debugPath.updatePathHandle(DemoPathHandle);
	DemoTranslationCenter = DemoPathHandle(2, 3);

}

// --------------
// -----Main-----
// --------------
int main(int argc, char** argv)
{
	initializeDemo();

	//uiThread = std::thread(uiDoWork);
	bool uiStarted = true;
	uiThread = std::thread(uiDoWork);
	robotControlThread = std::thread(robotDoWork);

	//hardloop testing
	while (true)
	{
		Sleep(1000);
		std::cout << "ave: "<<aveRobTime << " , max: "<<  maxRobTime<< std::endl;
		maxRobTime = 0;
		if (jointAngleMutex.try_lock())
		{
			std::cout << "command angles: " << std::endl;
			for (int i = 0; i < commandJointAngles.size(); i++)
			{
				std::cout << std::to_string(commandJointAngles[i]) << " , ";
			}
			std::cout << std::endl;
			jointAngleMutex.unlock();
		}
		if (jointAngleMutex.try_lock())
		{
			std::cout << "current angles: " << std::endl;
			for (int i = 0; i < lastJointAngles.size(); i++)
			{
				std::cout << std::to_string(lastJointAngles[i]) << " , ";
			}
			std::cout << std::endl;
			jointAngleMutex.unlock();
		}

		if (jointAngleMutex.try_lock())
		{
			Eigen::Matrix4f tempFlange = tempLastRobot.getJointFrame(7);
			jointAngleMutex.unlock();
			if (pathMutex.try_lock())
			{
				Eigen::Vector3f tempPt;
				double distToPath = debugPath.distToPath(tempFlange.col(3).head<3>(), tempPt);
				pathMutex.unlock();

				std::cout << "Path Deviation: " << std::endl;
				std::cout << sqrt(distToPath) << std::endl;
				std::cout << std::endl;
			}
		}
	}

	//Eigen::Vector3f p1 = rob.getJointFrame(6).col(3).
	//obs_createConfig();

	

	

	

	//TODO: temp debug
	//iiwa.gotoAngle(1, M_PI/4);
	//iiwa.gotoAngle(5, M_PI / 4);

	//boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizer = RoboVis::spawnRoboViewer();

	//while (!visualizer->wasStopped())
	//{
	//	if (abs(totalTranslation) > 250)
	//	{
	//		translationStep = -translationStep;
	//	}
	//	totalTranslation += translationStep;
	//	
	//	if (abs(totalRotation) > M_PI_4)
	//	{
	//		rotationStep = -rotationStep;
	//	}
	//	totalRotation += rotationStep;


	//	
	//	pathHandle(1, 3) += translationStep;
	//	RobotMath::rotateAboutAxis(pathHandle, pathHandle.col(3).head<3>(), pathHandle.col(0).head<3>(), rotationStep);
	//	
	//	debugPath.updatePathHandle(pathHandle);
	//	RoboVis::drawRobotPath(visualizer, debugPath.getTotalPath());
	//	RoboVis::drawRefFrame(pathHandle, "PathHandle", visualizer);
	//	
	//	
	//	
	//	//robTarget(1, 3) += translationStep;
	//	//RobotMath::rotateAboutAxis(robTarget, robTarget.col(3).head<3>(), robTarget.col(0).head<3>(), rotationStep);



	//	//RoboVis::drawRobotShell(visualizer, iiwa, false);
	//	//sphereKineToFrame(iiwa.getJointFrame(7), iiwa, visualizer);

	//	pcl::PointCloud<pcl::PointXYZ>::Ptr debugCloud(new pcl::PointCloud<pcl::PointXYZ>);
	//	//iiwa.gotoAngles(iiwaKinematics::sphereKineToFrame(robTarget, iiwa, debugCloud));


	//	//iiwa.gotoAngles(iiwaKinematics::sphereKineToFrame(debugPath.getCurrentPathPoint(), iiwa, debugCloud));
	//	//debugPath.incrementPath(true);
	//	bool pathComplete = false;
	//	iiwa.gotoAngles(iiwaKinematics::sphereKineToFrame(debugPath.getPathCommandPose(iiwa.getJointFrame(iiwa.getJointCount() - 1), 5, pathComplete, true),iiwa,debugCloud));
	//	RoboVis::drawDebugPoints(visualizer, debugCloud);
	//	RoboVis::drawRobotFrames(visualizer, iiwa);

	//	RoboVis::drawRobotShell(visualizer, iiwa, false);


	//	//RoboVis::drawRefFrame(robTarget, "robTarget", visualizer,25,35);

	//	visualizer->spinOnce(50);
	//}

	
	system("PAUSE");
	return 0;
}



//{
//	//speed test
//	int runs = 10000000;
//	double tempVal = 0;
//	std::cout << "starting speed test..." << std::endl;
//	auto t1 = Clock::now();
//	for (int i = 0; i < runs; i++)
//	{
//		if (abs(totalTranslation) > 250)
//		{
//			translationStep = -translationStep;
//		}
//		totalTranslation += translationStep;
//		robTarget(1, 3) += translationStep;
//
//		if (abs(totalRotation) > M_PI_4)
//		{
//			rotationStep = -rotationStep;
//		}
//		totalRotation += rotationStep;
//		RobotMath::rotateAboutAxis(robTarget, robTarget.col(3).head<3>(), robTarget.col(0).head<3>(), rotationStep);
//
//		tempVal += iiwaKinematics::sphereKineToFrame(robTarget, iiwa)[0];
//	}
//	auto t2 = Clock::now();
//	std::cout << tempVal << std::endl;
//	std::cout << "Speed test finished:" << std::endl;
//	std::cout << "Time for " << runs << " runs: " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count() << " microseconds" << std::endl;
//
//	system("Pause");
//}

void obs_createConfig()
{
	std::vector<pcl::PolygonMesh> robotMeshes = std::vector<pcl::PolygonMesh>();
	std::vector<Eigen::Matrix4f> robotFrames = std::vector<Eigen::Matrix4f>();

	{//J0
		pcl::PolygonMesh jointMesh;
		Eigen::Matrix4f jointFrame;
		jointFrame = Eigen::Matrix4f::Identity();
		//jointFrame = rotateAny(jointFrame, Eigen::RowVector3f(0, 0, 0), Eigen::RowVector3f(1, 0, 0), -M_PI_2);
		jointFrame(0, 3) += 0;
		jointFrame(1, 3) += 0;
		jointFrame(2, 3) += 147.5;

		std::cout << jointFrame << std::endl;
		robotFrames.push_back(jointFrame);
	}

	{//J1
		pcl::PolygonMesh jointMesh;
		Eigen::Matrix4f jointFrame;
		jointFrame = Eigen::Matrix4f::Identity();
		RobotMath::rotateAboutAxis(jointFrame, Eigen::RowVector3f(0, 0, 0), Eigen::RowVector3f(1, 0, 0), -M_PI_2);
		jointFrame(0, 3) += 0;
		jointFrame(1, 3) += -10;
		jointFrame(2, 3) += 360;

		std::cout << jointFrame << std::endl;
		robotFrames.push_back(jointFrame);
	}

	{//J2
		pcl::PolygonMesh jointMesh;
		Eigen::Matrix4f jointFrame;
		jointFrame = Eigen::Matrix4f::Identity();
		//jointFrame = rotateAny(jointFrame, Eigen::RowVector3f(0, 0, 0), Eigen::RowVector3f(1, 0, 0), -M_PI_2);
		jointFrame(0, 3) += 0;//-0.436244; //TODO: investigate this and check if it exists in IRL
		jointFrame(1, 3) += 0;
		jointFrame(2, 3) += 554.5;

		std::cout << jointFrame << std::endl;
		robotFrames.push_back(jointFrame);
	}

	{//J3
		pcl::PolygonMesh jointMesh;
		Eigen::Matrix4f jointFrame;
		jointFrame = Eigen::Matrix4f::Identity();
		RobotMath::rotateAboutAxis(jointFrame, Eigen::RowVector3f(0, 0, 0), Eigen::RowVector3f(1, 0, 0), M_PI_2);
		jointFrame(0, 3) += 0;
		jointFrame(1, 3) += 10.5;
		jointFrame(2, 3) += 780;

		std::cout << jointFrame << std::endl;
		robotFrames.push_back(jointFrame);
	}

	{//J4
		pcl::PolygonMesh jointMesh;
		Eigen::Matrix4f jointFrame;
		jointFrame = Eigen::Matrix4f::Identity();
		//jointFrame = rotateAny(jointFrame, Eigen::RowVector3f(0, 0, 0), Eigen::RowVector3f(1, 0, 0), M_PI_2);
		jointFrame(0, 3) += 0;
		jointFrame(1, 3) += 0;
		jointFrame(2, 3) += 954.5;

		std::cout << jointFrame << std::endl;
		robotFrames.push_back(jointFrame);
	}

	{//J5
		pcl::PolygonMesh jointMesh;
		Eigen::Matrix4f jointFrame;
		jointFrame = Eigen::Matrix4f::Identity();
		RobotMath::rotateAboutAxis(jointFrame, Eigen::RowVector3f(0, 0, 0), Eigen::RowVector3f(1, 0, 0), -M_PI_2);
		jointFrame(0, 3) += 0;
		jointFrame(1, 3) += -70.7;
		jointFrame(2, 3) += 1180;

		std::cout << jointFrame << std::endl;
		robotFrames.push_back(jointFrame);
	}

	{//J6
		pcl::PolygonMesh jointMesh;
		Eigen::Matrix4f jointFrame;
		jointFrame = Eigen::Matrix4f::Identity();
		//jointFrame = rotateAny(jointFrame, Eigen::RowVector3f(0, 0, 0), Eigen::RowVector3f(1, 0, 0), -M_PI_2);
		jointFrame(0, 3) += 0;
		jointFrame(1, 3) += 0;
		jointFrame(2, 3) += 1270.9;

		std::cout << jointFrame << std::endl;
		robotFrames.push_back(jointFrame);
	}

	{//J7
		pcl::PolygonMesh jointMesh;
		Eigen::Matrix4f jointFrame;
		jointFrame = Eigen::Matrix4f::Identity();
		//jointFrame = rotateAny(jointFrame, Eigen::RowVector3f(0, 0, 0), Eigen::RowVector3f(1, 0, 0), -M_PI_2);
		jointFrame(0, 3) += 0;
		jointFrame(1, 3) += 0;
		jointFrame(2, 3) += 1332;//to face of flange

		std::cout << jointFrame << std::endl;
		robotFrames.push_back(jointFrame);
	}

	fileIO::saveRobotConfig(robotFrames);
	//RoboVis::fixturedMeshViewer(robotMeshes, robotFrames);
}




//Eigen::Matrix4f arrayToMat(double input[])
//{
//	Eigen::Matrix4f output = Eigen::Matrix4f::Identity();
//	for (int i = 0; i < 16; i++)
//	{
//		output(floor(i / 4), i % 4) = input[i];
//	}
//	return output;
//}
//void transformMesh(pcl::PolygonMesh &mesh, Eigen::Matrix4f tranformation)
//{
//	pcl::PointCloud<pcl::PointXYZ> cloud;
//	pcl::fromPCLPointCloud2(mesh.cloud, cloud);
//	pcl::transformPointCloud(cloud, cloud, tranformation);
//	pcl::toPCLPointCloud2(cloud, mesh.cloud);
//}
//
//void eigenTesting()
//{
//	double temp[] = {
//		1,2,3,10,
//		4,5,6,11,
//		7,8,9,12,
//		13,14,15,16
//	};
//	Eigen::Matrix4f test = arrayToMat(temp);
//	std::cout << Eigen::Vector3f(test.col(3).head<3>()) << std::endl;
//	std::cout << Eigen::Vector3f(test.col(3).head(3)) << std::endl;//same but slower ( but can have variable input)
//	system("PAUSE");
//	return;
//}