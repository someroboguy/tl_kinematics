#pragma once
#include <iostream>
#include <vector>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/common/distances.h>

#include <chrono>

typedef std::chrono::high_resolution_clock Clock;

class RobotMath
{
public:
	//applies transformation to targetFrame and returns the transformation applied
	static Eigen::Matrix4f rotateAboutAxis(Eigen::Matrix4f &targetFrame, Eigen::Vector3f pt, Eigen::Vector3f dir, double angleRad)
	{
		Eigen::Affine3f A = Eigen::Translation3f(pt) * Eigen::AngleAxisf(angleRad, dir) * Eigen::Translation3f(-pt);
		targetFrame = (A * Eigen::Affine3f(targetFrame)).matrix();
		return A.matrix();
	}

	static double seg2segDist(Eigen::Vector3f d1, Eigen::Vector3f d2, Eigen::Vector3f p1, Eigen::Vector3f p2, Eigen::Vector3f &outMidPt);
};

class RobotJoint
{
public:
	RobotJoint(Eigen::Matrix4f zeroAngleJoint)
	{
		actuationFrame = zeroAngleJoint;
		currentAngle = 0;
	}

	//Applies the rotation to the joint and returns the transformation applied
	Eigen::Matrix4f rotate(double angle)
	{
		currentAngle += angle;
		return RobotMath::rotateAboutAxis(
			actuationFrame,
			Eigen::Vector3f(actuationFrame(0, 3), actuationFrame(1, 3), actuationFrame(2, 3)),
			Eigen::Vector3f(actuationFrame(0, 2), actuationFrame(1, 2), actuationFrame(2, 2)),
			angle
		);
	}

	void moveAsChild(Eigen::Matrix4f parentTransformation)
	{
		actuationFrame = parentTransformation * actuationFrame;
	}


	Eigen::Matrix4f getActuationFrame() { return actuationFrame; }
	double getCurrentAngle() { return currentAngle; }
	
private:
	Eigen::Matrix4f actuationFrame;
	double currentAngle;
};

//group of connected joints.  The final frame is the flange offset from the final joint and therefore can not be actuated
class Robot
{
public:
	Robot(std::vector<Eigen::Matrix4f> zeroAngleJoints)
	{
		roboJoints = std::vector<RobotJoint>();
		for (int i = 0; i < zeroAngleJoints.size(); i++)
		{
			roboJoints.push_back(RobotJoint(zeroAngleJoints[i]));
		}
	}
	Robot() {}

	void gotoAngles(std::vector<double> targetAngles)
	{
		for (int i = 0; i < roboJoints.size()-1; i++)
		{
			gotoAngle(i, targetAngles[i]);
		}
	}
	
	void gotoAngle(int joint, double angle)
	{
		if (joint >= roboJoints.size() - 1) { return; }
		Eigen::Matrix4f childTransform = roboJoints[joint].rotate(angle - roboJoints[joint].getCurrentAngle());
		for (int i = joint + 1; i < roboJoints.size(); i++)
		{
			roboJoints[i].moveAsChild(childTransform);
		}
	}


	Eigen::Matrix4f getJointFrame(int index)
	{
		return roboJoints[index].getActuationFrame();
	}
	double getJointAngle(int index)
	{
		return roboJoints[index].getCurrentAngle();
	}

	std::vector<double> getJointAngles()
	{
		std::vector<double> outAngles = std::vector<double>();
		for (int i = 0; i < roboJoints.size(); i++)
		{
			outAngles.push_back(roboJoints[i].getCurrentAngle());
		}
		return outAngles;
	}

	int getJointCount() { return roboJoints.size(); }
	
private:
	std::vector<RobotJoint> roboJoints;
};

//this class leverages symetries in the design of the kuka iiwa to simplify inverse kinematic calculations
class iiwaKinematics
{
public:
	static void initializeKinematics(Robot robotAtNull);
	static std::vector<double> iiwaKinematics::sphereKineToFrame(Eigen::Matrix4f targetFrame, Robot rob, pcl::PointCloud<pcl::PointXYZ>::Ptr debugCloud = nullptr);

private:
	static Eigen::Vector3f targetOffsetToPiv;
	static Eigen::Vector3f basePivPt;
	static double elbowALength;
	static double elbowBLength;
	static bool kinematicsInitialized;

};

class RobotPath
{
public:
	RobotPath(std::vector<pcl::PointXYZ> originCenteredPath, Eigen::Matrix4f defaultToolPose)
	{
		pathHandle = Eigen::Matrix4f::Identity();
		pathPoints = std::vector<Eigen::Matrix4f>();
		for (int i = 0; i < originCenteredPath.size(); i++)
		{
			Eigen::Matrix4f tempFrame = defaultToolPose;
			tempFrame(0, 3) = originCenteredPath[i].x;
			tempFrame(1, 3) = originCenteredPath[i].y;
			tempFrame(2, 3) = originCenteredPath[i].z;
			pathPoints.push_back(tempFrame);
		}
	}
	RobotPath() {}
	//RobotPath(std::vector<pcl::PointNormal> originCenteredPath);
	//RobotPath(std::vector<Eigen::Matrix4f> originCenteredPath);

	void optimizePathReach(Eigen::Matrix4f robotBase)//TODO: this needs to be optimized to better handle tool variation
	{
	}


	void updatePathHandle(Eigen::Matrix4f updatedHandle)
	{
		//Eigen::Matrix4f relativeTransform = pathHandle.inverse() * updatedHandle;
		for (int i = 0; i < pathPoints.size(); i++)
		{
			pathPoints[i] = updatedHandle * (pathHandle.inverse() * pathPoints[i]);
		}
		pathHandle = updatedHandle;
	}

	bool incrementPath(bool loopPath = false)
	{
		currentPathIndex++;
		if (currentPathIndex >= pathPoints.size()) 
		{ 
			if (loopPath) 
			{
				currentPathIndex = 0;
			}
			return true; 
		}
		return false;
	}

	Eigen::Matrix4f getCurrentPathPoint()
	{
		return pathPoints[currentPathIndex];
	}

	Eigen::Matrix4f getPathCommandPose(Eigen::Matrix4f currentRobotFlangeFrame, double targetOffsetDistance, double cornerRadius, bool &pathComplete, bool loop = false);
	
	//TODO:This will become unstable if the (tartet speed/pathstep size) gets too high
	Eigen::Matrix4f simpleSpeedPathing(Eigen::Matrix4f currentRobotFlangeFrame, double targetSpeed_MMpS, bool &pathComplete, bool loop = false);
	
	void resetSpeedClock()
	{
		speedClock = Clock::now();
	}


	//TODO: if this is going to be used in a fast loop, it should be optimized
	//TODO: time this alone with to get a feel for required processing
	double distToPath(Eigen::Vector3f testPt, Eigen::Vector3f &closestPt)
	{
		double minDist = 100000000; //would fail if min dist > 10000mm lol
		int bestIndex = 0;
		for (int i = 0; i < pathPoints.size(); i++)
		{
			int nextIndex = i + 1;
			if (nextIndex == pathPoints.size()) { nextIndex = 0; }

			//double testDist = (testPt - Eigen::Vector3f(pathPoints[i].col(3).head<3>())).squaredNorm(); old pt to pt
			Eigen::Vector3f pt1 = pathPoints[i].col(3).head<3>();
			Eigen::Vector3f pt2 = pathPoints[nextIndex].col(3).head<3>();
			Eigen::Vector3f dir = pt2 - pt1;
			dir.normalize();

			Eigen::Vector4f tempTestPt = Eigen::Vector4f(testPt(0), testPt(1), testPt(2), 0);
			Eigen::Vector4f tempLinPt = Eigen::Vector4f(pt1(0), pt1(1), pt1(2), 0);
			Eigen::Vector4f tempLinDir = Eigen::Vector4f(dir(0), dir(1), dir(2), 0);
			double testDist = pcl::sqrPointToLineDistance(tempTestPt, tempLinPt, tempLinDir);
			if (testDist < minDist)
			{
				minDist = testDist;
				bestIndex = i;
			}
		}
		closestPt = Eigen::Vector3f(pathPoints[bestIndex].col(3).head<3>());
		return minDist;
	}


	Eigen::Matrix4f errorCompensatedPathing();

	std::vector<Eigen::Matrix4f> getTotalPath()
	{
		return pathPoints;
	}

	Eigen::Matrix4f getPathHandle() { return pathHandle; }

private:
	Eigen::Matrix4f pathHandle;
	std::vector<Eigen::Matrix4f> pathPoints;
	int currentPathIndex = 0;


	Eigen::Matrix4f lastSpeedFrame = Eigen::Matrix4f::Identity();
	Clock::time_point speedClock = Clock::now();
	bool speedPathStarted = false;

	//TODO: this currently only smooths position, orientation would be nice to add
	Eigen::Matrix4f smoothedCommandFrame = Eigen::Matrix4f::Identity();
	double filterCoeff = 1;//.25
	bool firstCommandFlag = true;

	long loopsPerPathPoint = 0;

	Eigen::Matrix4f interpolateFrames(Eigen::Matrix4f frame1, Eigen::Matrix4f frame2, double interpolationDistance);
};

