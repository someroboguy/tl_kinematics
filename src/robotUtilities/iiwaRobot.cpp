#include "iiwaRobot.h"

Eigen::Vector3f iiwaKinematics::targetOffsetToPiv = Eigen::Vector3f();
Eigen::Vector3f iiwaKinematics::basePivPt = Eigen::Vector3f();
double iiwaKinematics::elbowALength = 0;
double iiwaKinematics::elbowBLength = 0;
bool iiwaKinematics::kinematicsInitialized = false;

double RobotMath::seg2segDist(Eigen::Vector3f d1, Eigen::Vector3f d2, Eigen::Vector3f p1, Eigen::Vector3f p2, Eigen::Vector3f &outMidPt)
{
	//TODO: come back and actually learn/ understand this as that understanding may be usefull later
	//See this as reference for the equations:
	//https://en.wikipedia.org/wiki/Skew_lines#Distance

	d1.normalize();
	d2.normalize();

	Eigen::Vector3f n2 = d2.cross(d1.cross(d2));
	Eigen::Vector3f n1 = d1.cross(d2.cross(d1));

	Eigen::Vector3f closestPt1 = p1 + (((p2 - p1).dot(n2)) / (d1.dot(n2))) * d1;
	Eigen::Vector3f closestPt2 = p2 + (((p1 - p2).dot(n1)) / (d2.dot(n1))) * d2;

	outMidPt = (closestPt1 + closestPt2) / 2;
	Eigen::Vector3f difference = closestPt1 - closestPt2;
	return difference.norm();
}


void iiwaKinematics::initializeKinematics(Robot robotAtNull)
{
	//calculate target offset
	Eigen::Vector3f tempHandPivPt;
	{
		Eigen::Vector3f p1 = robotAtNull.getJointFrame(6).col(3).head<3>();
		Eigen::Vector3f d1 = robotAtNull.getJointFrame(6).col(2).head<3>();

		Eigen::Vector3f p2 = robotAtNull.getJointFrame(5).col(3).head<3>();
		Eigen::Vector3f d2 = robotAtNull.getJointFrame(5).col(2).head<3>();

		RobotMath::seg2segDist(d1, d2, p1, p2, tempHandPivPt);
	}
	targetOffsetToPiv = tempHandPivPt - robotAtNull.getJointFrame(7).col(3).head<3>();
	//transform into J7 space
	targetOffsetToPiv = robotAtNull.getJointFrame(7).block<3, 3>(0, 0) * targetOffsetToPiv;


	//base pivot
	{
		Eigen::Vector3f p1 = robotAtNull.getJointFrame(0).col(3).head<3>();
		Eigen::Vector3f d1 = robotAtNull.getJointFrame(0).col(2).head<3>();

		Eigen::Vector3f p2 = robotAtNull.getJointFrame(1).col(3).head<3>();
		Eigen::Vector3f d2 = robotAtNull.getJointFrame(1).col(2).head<3>();

		RobotMath::seg2segDist(d1, d2, p1, p2, basePivPt);
	}

	//elbow A
	{
		Eigen::Vector3f p1 = robotAtNull.getJointFrame(2).col(3).head<3>();
		Eigen::Vector3f d1 = robotAtNull.getJointFrame(2).col(2).head<3>();

		Eigen::Vector3f p2 = robotAtNull.getJointFrame(3).col(3).head<3>();
		Eigen::Vector3f d2 = robotAtNull.getJointFrame(3).col(2).head<3>();

		Eigen::Vector3f tempPt;
		RobotMath::seg2segDist(d1, d2, p1, p2, tempPt);
		elbowALength = (basePivPt - tempPt).norm();
	}
	//elbow B
	{
		Eigen::Vector3f p1 = robotAtNull.getJointFrame(4).col(3).head<3>();
		Eigen::Vector3f d1 = robotAtNull.getJointFrame(4).col(2).head<3>();

		Eigen::Vector3f p2 = robotAtNull.getJointFrame(3).col(3).head<3>();
		Eigen::Vector3f d2 = robotAtNull.getJointFrame(3).col(2).head<3>();
		
		Eigen::Vector3f tempPt;
		RobotMath::seg2segDist(d1, d2, p1, p2, tempPt);
		elbowBLength = (tempHandPivPt - tempPt).norm();
	}


	kinematicsInitialized = true;
}


std::vector<double> iiwaKinematics::sphereKineToFrame(Eigen::Matrix4f targetFrame, Robot rob, pcl::PointCloud<pcl::PointXYZ>::Ptr debugCloud)
{
	std::vector<double> jointAngles = std::vector<double>();
	
	for (int i = 0; i < 7; i++)
	{
		jointAngles.push_back(0);
	}
	if (!kinematicsInitialized) { std::cout << "kinematics not initialized" << std::endl; return rob.getJointAngles(); }


	//Estimate hand piv pt using offset and current target frame
	Eigen::Vector3f handPivPt = targetFrame.col(3).head<3>();//This is needed in loop
	handPivPt = handPivPt + targetOffsetToPiv(2)*targetFrame.col(2).head<3>();

	//calculate side lengths of connecting triangle
	Eigen::Vector3f basePivToHandPiv = handPivPt - basePivPt;
	double C = basePivToHandPiv.norm();

	if (C > elbowALength + elbowBLength) { std::cout << "out of reach" << std::endl; return rob.getJointAngles(); }

	//calculate triangle angles to solve (using law of cosines)
	double elbowAngleFromDiag = acos((elbowBLength * elbowBLength - C * C - elbowALength * elbowALength) / (-2 * elbowALength*C));


	//project elbow piv point
	basePivToHandPiv.normalize();
	Eigen::Vector3f trianglePlaneVector = Eigen::Vector3f(rob.getJointFrame(0).col(2).head<3>()).cross(basePivToHandPiv);
	trianglePlaneVector.normalize();
	Eigen::Vector3f basePivToElbowVec = Eigen::AngleAxis<float>(-elbowAngleFromDiag, trianglePlaneVector) * basePivToHandPiv;
	Eigen::Vector3f elbowPnt = elbowALength * basePivToElbowVec + basePivPt;

	if (debugCloud != NULL)
	{
		//double distA = (basePivPt - elbowPnt).norm();
		//double distB = (handPivPt - elbowPnt).norm();
		//double distC = (handPivPt - basePivPt).norm();
		debugCloud->points.push_back(pcl::PointXYZ(handPivPt(0), handPivPt(1), handPivPt(2)));
		debugCloud->points.push_back(pcl::PointXYZ(basePivPt(0), basePivPt(1), basePivPt(2)));
		debugCloud->points.push_back(pcl::PointXYZ(elbowPnt(0), elbowPnt(1), elbowPnt(2)));
	}

	Eigen::Vector3f ElbowToHandPivVec = handPivPt - elbowPnt;

	//solve all angles of robot to satistfy calculated points
	//J0
	{
		Eigen::Vector3f rotAxis = rob.getJointFrame(0).col(2).head<3>();
		Eigen::Vector3f currentDir = rob.getJointFrame(1).col(2).head<3>();
		Eigen::Vector3f targetDir = rotAxis.cross(basePivToElbowVec);
		targetDir.normalize();
		if (pcl::getAngle3D(currentDir, targetDir) > M_PI_2)
		{
			targetDir = -targetDir;
		}

		Eigen::Vector3f difVec = currentDir.cross(targetDir);
		double angleToTarget = asin(difVec.norm());
		if (difVec.dot(rotAxis) < 0) { angleToTarget = -angleToTarget; }
		jointAngles[0] = rob.getJointAngle(0) + angleToTarget;
		rob.gotoAngle(0, jointAngles[0]);//only modifies the local copy of rob
	}

	//J1
	{
		Eigen::Vector3f rotAxis = rob.getJointFrame(1).col(2).head<3>();
		Eigen::Vector3f currentDir = rob.getJointFrame(2).col(2).head<3>();
		Eigen::Vector3f targetDir = basePivToElbowVec;
		targetDir.normalize();
		Eigen::Vector3f difVec = currentDir.cross(targetDir);
		double angleToTarget = asin(difVec.norm());
		if (difVec.dot(rotAxis) < 0) { angleToTarget = -angleToTarget; }
		jointAngles[1] = rob.getJointAngle(1) + angleToTarget;
		rob.gotoAngle(1, jointAngles[1]);//only modifies the local copy of rob
	}

	//J2
	{
		Eigen::Vector3f rotAxis = rob.getJointFrame(2).col(2).head<3>();
		Eigen::Vector3f currentDir = -rob.getJointFrame(3).col(2).head<3>();//TODO: had to sign flip this to match arbitrary config decisions
		Eigen::Vector3f targetDir = rotAxis.cross(ElbowToHandPivVec);
		targetDir.normalize();
		if (pcl::getAngle3D(currentDir, targetDir) > M_PI_2)
		{
			targetDir = -targetDir;
		}

		Eigen::Vector3f difVec = currentDir.cross(targetDir);
		double angleToTarget = asin(difVec.norm());
		if (difVec.dot(rotAxis) < 0) { angleToTarget = -angleToTarget; }
		jointAngles[2] = rob.getJointAngle(2) + angleToTarget;
		rob.gotoAngle(2, jointAngles[2]);//only modifies the local copy of rob
	}

	//J3
	{
		Eigen::Vector3f rotAxis = rob.getJointFrame(3).col(2).head<3>();
		Eigen::Vector3f currentDir = rob.getJointFrame(4).col(2).head<3>();
		Eigen::Vector3f targetDir = ElbowToHandPivVec;
		targetDir.normalize();
		Eigen::Vector3f difVec = currentDir.cross(targetDir);
		double angleToTarget = asin(difVec.norm());
		if (difVec.dot(rotAxis) < 0) { angleToTarget = -angleToTarget; }
		jointAngles[3] = rob.getJointAngle(3) + angleToTarget;
		rob.gotoAngle(3, jointAngles[3]);//only modifies the local copy of rob
	}

	//J4
	{
		Eigen::Vector3f rotAxis = rob.getJointFrame(4).col(2).head<3>();
		Eigen::Vector3f currentDir = rob.getJointFrame(5).col(2).head<3>();
		Eigen::Vector3f targetDir = rotAxis.cross(targetFrame.col(2).head<3>());
		targetDir.normalize();
		if (pcl::getAngle3D(currentDir, targetDir) > M_PI_2)
		{
			targetDir = -targetDir;
		}

		Eigen::Vector3f difVec = currentDir.cross(targetDir);
		double angleToTarget = asin(difVec.norm());
		if (difVec.dot(rotAxis) < 0) { angleToTarget = -angleToTarget; }
		jointAngles[4] = rob.getJointAngle(4) + angleToTarget;
		rob.gotoAngle(4, jointAngles[4]);//only modifies the local copy of rob
	}

	//J5
	{
		Eigen::Vector3f rotAxis = rob.getJointFrame(5).col(2).head<3>();
		Eigen::Vector3f currentDir = rob.getJointFrame(6).col(2).head<3>();
		Eigen::Vector3f targetDir = targetFrame.col(2).head<3>();
		targetDir.normalize();
		Eigen::Vector3f difVec = currentDir.cross(targetDir);
		double angleToTarget = asin(difVec.norm());
		if (difVec.dot(rotAxis) < 0) { angleToTarget = -angleToTarget; }
		jointAngles[5] = rob.getJointAngle(5) + angleToTarget;
		rob.gotoAngle(5, jointAngles[5]);//only modifies the local copy of rob
	}

	//J6
	{
		Eigen::Vector3f rotAxis = rob.getJointFrame(6).col(2).head<3>();
		Eigen::Vector3f currentDir = rob.getJointFrame(6).col(0).head<3>();
		Eigen::Vector3f targetDir = targetFrame.col(0).head<3>();
		targetDir.normalize();
		Eigen::Vector3f difVec = currentDir.cross(targetDir);
		double angleToTarget = asin(difVec.norm());
		if (difVec.dot(rotAxis) < 0) { angleToTarget = -angleToTarget; }
		jointAngles[6] = rob.getJointAngle(6) + angleToTarget;
		rob.gotoAngle(6, jointAngles[6]);//only modifies the local copy of rob
	}

	//rotate the elbow pt about the center line in both directions to optimize (mininimize largest joint angle)

	//TODO: temp debug
	/*std::cout << "joint angles: " << std::endl;
	for (int i = 0; i < jointAngles.size(); i++)
	{
		std::cout << std::to_string(jointAngles[i]) << " , ";
	}
	std::cout << std::endl;*/

	return jointAngles;
}

Eigen::Matrix4f RobotPath::getPathCommandPose(Eigen::Matrix4f currentRobotFlangeFrame, double targetOffsetDistance, double cornerRadius, bool &pathComplete, bool loop)
{
	//This is an arbitrary value to relate angular change to linear change
	//TODO:this may need tuning
	double rotPerMM = M_PI / 16; //approximately 12 degree per mm

	Eigen::Matrix4f tFrame = pathPoints[currentPathIndex];

	//Two "distances" need to be checked: euclidean distance and angular distance between frames
	//This is neccesary to avoid seemingly rapid accelerations in movement for TCP rotations as compared to normal euclidian motions
	//interpolation towards next frame is determined by whichever (angle or translation) is the limiting factor
	double euclidDist = (tFrame.col(3).head<3>() - currentRobotFlangeFrame.col(3).head<3>()).norm();
	double zAngleDiv = pcl::getAngle3D(Eigen::Vector3f(tFrame.col(2).head<3>()), Eigen::Vector3f(currentRobotFlangeFrame.col(2).head<3>()));
	double yAngleDiv = pcl::getAngle3D(Eigen::Vector3f(tFrame.col(1).head<3>()), Eigen::Vector3f(currentRobotFlangeFrame.col(1).head<3>()));
	double maxAngleDiv = std::max(zAngleDiv, yAngleDiv);
	double angleDist = maxAngleDiv / rotPerMM;

	double maxTotalDiv = std::max(euclidDist, angleDist);
	
	
	if (maxTotalDiv > cornerRadius) //interpolate between current position and target
	{
		double travelRatio = targetOffsetDistance / maxTotalDiv;
		
		Eigen::Matrix4f iFrame = interpolateFrames(currentRobotFlangeFrame, tFrame, travelRatio);

		if (firstCommandFlag)
		{
			firstCommandFlag = false;
			smoothedCommandFrame = iFrame;
		}
		else
		{
			Eigen::Matrix4f tempFrame = iFrame;

			tempFrame(0, 3) = smoothedCommandFrame(0, 3) * (1.0 - filterCoeff) + iFrame(0, 3) * filterCoeff;
			tempFrame(1, 3) = smoothedCommandFrame(1, 3) * (1.0 - filterCoeff) + iFrame(1, 3) * filterCoeff;
			tempFrame(2, 3) = smoothedCommandFrame(2, 3) * (1.0 - filterCoeff) + iFrame(2, 3) * filterCoeff;
			smoothedCommandFrame = tempFrame;
		}

		/*std::cout << "----------------------------" << std::endl;
		std::cout << "Current robot Frame" << std::endl;
		std::cout << currentRobotFlangeFrame << std::endl;
		std::cout << "Target path Frame" << std::endl;
		std::cout << tFrame << std::endl;
		std::cout << "Interpolated Frame" << std::endl;
		std::cout << iFrame << std::endl;
		std::cout << "Smoothed Frame" << std::endl;
		std::cout << smoothedCommandFrame << std::endl;*/
		loopsPerPathPoint += 1;
		pathComplete = false;
		return smoothedCommandFrame;
	}
	else //increment path index
	{
		if (currentPathIndex == pathPoints.size() - 1) //path finished, stop at end of path
		{
			std::cout << "Path finished" << std::endl;
			//std::cout << currentRobotFlangeFrame << std::endl;
			if (!loop)
			{
				pathComplete = true;
				return currentRobotFlangeFrame;
			}
			else
			{
				currentPathIndex = 0;
				return getPathCommandPose(currentRobotFlangeFrame, targetOffsetDistance, cornerRadius, pathComplete, loop);
			}
			
		}
		else //path point reached, index to next path point
		{
			//std::cout << "lppp: " << loopsPerPathPoint << std::endl;
			loopsPerPathPoint = 0;
			currentPathIndex += 1;
			return getPathCommandPose(currentRobotFlangeFrame, targetOffsetDistance, cornerRadius, pathComplete, loop);
		}
	}
}

Eigen::Matrix4f RobotPath::simpleSpeedPathing(Eigen::Matrix4f currentRobotFlangeFrame, double targetSpeed_MMpS, bool &pathComplete, bool loop)
{
	double deltaT = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now() - speedClock).count();
	deltaT = deltaT / 1000000.0;//convert to seconds
	speedClock = Clock::now();

	//on first entry, set last frame to robot flange to start the motion
	if (lastSpeedFrame == Eigen::Matrix4f::Identity()) { lastSpeedFrame = currentRobotFlangeFrame; }

	Eigen::Matrix4f outFrame = Eigen::Matrix4f::Identity();

	//speed pathing requires regular update to operate correctly, clamp deltaT to .1 s and report a warning to terminal
	if (deltaT > 0.1)
	{
		deltaT = 0.1;
		std::cout << "speed pathing clamped to max travel due to infrequent update" << std::endl;
	}


	if (!speedPathStarted) //Allows robot to approach prior to starting path
	{
		Eigen::Vector3f curPos = currentRobotFlangeFrame.col(3).head<3>();
		Eigen::Vector3f startPos = pathPoints[0].col(3).head<3>();
		double totalDistance = (curPos - startPos).norm();

		if (totalDistance < (targetSpeed_MMpS * deltaT)*10)
		{
			speedPathStarted = true;
		}

		outFrame = interpolateFrames(lastSpeedFrame, pathPoints[0], (targetSpeed_MMpS*deltaT) / totalDistance);
	}
	else
	{
		//std::cout << deltaT << std::endl;
		int nextIndex = currentPathIndex + 1;
		if (nextIndex >= pathPoints.size())
		{
			if (loop)
			{
				nextIndex = 0;
			}
			else
			{
				pathComplete = true;
				return currentRobotFlangeFrame;
			}
		}
		
		double distanceToNextTarget = (Eigen::Vector3f(lastSpeedFrame.col(3).head<3>()) - Eigen::Vector3f(pathPoints[nextIndex].col(3).head<3>())).norm();
		double travRatio = (targetSpeed_MMpS*deltaT) / distanceToNextTarget;
		outFrame = interpolateFrames(lastSpeedFrame, pathPoints[nextIndex], travRatio);
		if (travRatio > 1) { currentPathIndex = nextIndex; }
	}
	lastSpeedFrame = outFrame;
	return outFrame;
}

Eigen::Matrix4f RobotPath::interpolateFrames(Eigen::Matrix4f frame1, Eigen::Matrix4f frame2, double interpolationFraction)
{
	Eigen::Vector3f interpolatedPoint = frame1.col(3).head<3>() + (frame2.col(3).head<3>() - frame1.col(3).head<3>()) * interpolationFraction;

	Eigen::Vector3f interpolatedZAxis;
	{
		Eigen::Vector3f currentZ = Eigen::Vector3f(frame1.col(2).head<3>());
		Eigen::Vector3f targetZ = Eigen::Vector3f(frame2.col(2).head<3>());
		double zAngle = pcl::getAngle3D(currentZ, targetZ);
		if (zAngle == M_PI)
		{
			//std::cout << "Z perpendicular target correction applied" << std::endl;
			//rotate slightly away from origin to break perpendicular alignment
			Eigen::Vector3f originVec = -Eigen::Vector3f(frame2.col(3).head<3>());
			originVec.normalize();
			Eigen::Vector3f tipAxis = originVec.cross(targetZ);
			tipAxis.normalize();
			targetZ = Eigen::AngleAxis<float>(M_PI / 16, tipAxis) * targetZ;

		}
		Eigen::Vector3f rotDir = currentZ.cross(targetZ);
		rotDir.normalize();
		interpolatedZAxis = Eigen::AngleAxis<float>(interpolationFraction*zAngle, rotDir) * currentZ;
		interpolatedZAxis.normalize();
	}


	Eigen::Vector3f interpolatedXAxis;
	Eigen::Vector3f interpolatedYAxis;
	{
		Eigen::Vector3f currentX = Eigen::Vector3f(frame1.col(0).head<3>());
		Eigen::Vector3f targetX = Eigen::Vector3f(frame2.col(0).head<3>());
		double xAngle = pcl::getAngle3D(currentX, targetX);
		if (xAngle == M_PI)
		{
			//std::cout << "X perpendicular target correction applied" << std::endl;
			//rotate slightly away from origin to break perpendicular alignment
			Eigen::Vector3f originVec = -Eigen::Vector3f(frame2.col(3).head<3>());
			originVec.normalize();
			Eigen::Vector3f tipAxis = originVec.cross(targetX);
			tipAxis.normalize();
			targetX = Eigen::AngleAxis<float>(M_PI / 16, tipAxis) * targetX;

		}
		Eigen::Vector3f rotDir = currentX.cross(targetX);
		rotDir.normalize();
		interpolatedXAxis = Eigen::AngleAxis<float>(interpolationFraction*xAngle, rotDir) * currentX;
		interpolatedXAxis.normalize();
	}

	//double cross to ensure perpendicularity is maintained
	interpolatedYAxis = interpolatedZAxis.cross(interpolatedXAxis);
	interpolatedYAxis.normalize();

	interpolatedXAxis = interpolatedYAxis.cross(interpolatedZAxis);
	interpolatedXAxis.normalize();

	//TODO: there has to be a way to inject into rows of matrix4f... but idk how

	Eigen::Matrix4f iFrame = Eigen::Matrix4f::Identity();
	iFrame(0, 0) = interpolatedXAxis(0);
	iFrame(1, 0) = interpolatedXAxis(1);
	iFrame(2, 0) = interpolatedXAxis(2);

	iFrame(0, 1) = interpolatedYAxis(0);
	iFrame(1, 1) = interpolatedYAxis(1);
	iFrame(2, 1) = interpolatedYAxis(2);

	iFrame(0, 2) = interpolatedZAxis(0);
	iFrame(1, 2) = interpolatedZAxis(1);
	iFrame(2, 2) = interpolatedZAxis(2);

	iFrame(0, 3) = interpolatedPoint(0);
	iFrame(1, 3) = interpolatedPoint(1);
	iFrame(2, 3) = interpolatedPoint(2);

	return iFrame;
}