#include "FRIClient.h"

FRIClient::FRIClient()
{
	this->isFRIClientConnected = false;
	hostname = "192.170.10.2";//"192.168.125.10";//NULL;
	port = DEFAULT_PORTID;
	killTSetMutex = new std::mutex();
}

bool FRIClient::connect(void)
{

	trafoClient = new LBRJointOverlayClient();
	connection = new KUKA::FRI::UdpConnection();

	app = new KUKA::FRI::ClientApplication(*connection, *trafoClient);

	isFRIClientConnected = false;
	FRIClientThread_ =
		std::thread(&FRIClient::internalConnectFRIClient_, this);
	
	while (!this->isFRIClientConnected) {
		std::cout << "waiting for better fri connection" << std::endl;
		boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));
	}

	return true;
}

bool FRIClient::isConnected(void)
{
	return isFRIClientConnected;
}

bool FRIClient::isFRIInActiveMode(void)
{
	return trafoClient->getIsInCommandMode();
}

bool FRIClient::updateAngles(std::vector<double> angles)
{
	if (isFRIClientConnected && angles.size() == NUM_JOINTS) {
		trafoClient->updateAngles(angles);
		return true;
	}
	return false;
}

double FRIClient::getSecSampleTime(void)
{
	return trafoClient->getSampleTime();
}

std::vector<double> FRIClient::getMeasuredJointAngs(bool * wasUpdated)
{
	return trafoClient->getMeasuredJointPosition(wasUpdated);
}

std::vector<double> FRIClient::getMeasuredJointAngs(void)
{
	bool wasUpdate;
	return trafoClient->getMeasuredJointPosition(&wasUpdate);
}

bool FRIClient::internalConnectFRIClient_(void)
{
	std::cout << "pre app->connect" << std::endl;
	this->app->connect(port, hostname);
	std::cout << "post app->Connect" << std::endl;
	bool success = true;
	bool breakout = false;


	int sessionState = 0;
	if (breakout) {
		std::cout << "Breakout was true" << std::endl;
	}
	else {
		std::cout << "Breakout was false" << std::endl;
	}
	std::cout << "Pre SessionState <= Monitoring_Wait while loop" << std::endl;
	while (sessionState <= KUKA::FRI::MONITORING_WAIT) {
		success = app->step();
		sessionState = this->trafoClient->robotState().getSessionState();

		this->killTSetMutex->lock();
		breakout = killT;
		this->killTSetMutex->unlock();
		if (breakout) {
			std::cout << "exiting because of breakout" << std::endl;
			break;
		}
	}
	std::cout << "Post SessionState <= Monitoring_Wait while loop" << std::endl;
	if (breakout) {
		std::cout << "Breakout was true" << std::endl;
	}
	else {
		std::cout << "Breakout was false" << std::endl;
	}
	this->isFRIClientConnected = true;

	if (!breakout) {
		while (success) {
			success = app->step();
			try {
				sessionState = this->trafoClient->robotState().getSessionState();
			}
			catch (std::exception e) {
				std::cout << "threw on sessionstate" << std::endl;
			}
			this->killTSetMutex->lock();
			breakout = killT;
			this->killTSetMutex->unlock();
			if (breakout) {
				std::cout << "exiting because of breakout" << std::endl;
				break;
			}
		}
	}
	std::cout << "exiting if(!breakout)" << std::endl;
	if (breakout) {
		std::cout << "Breakout was true" << std::endl;
	}
	else {
		std::cout << "Breakout was false" << std::endl;
	}
	app->disconnect();
	this->isFRIClientConnected = false;
	return false;
}

