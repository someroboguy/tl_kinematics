/**

The following license terms and conditions apply, unless a redistribution
agreement or other license is obtained by KUKA Roboter GmbH, Augsburg, Germany.

SCOPE

The software �KUKA Sunrise.Connectivity FRI Client SDK� is targeted to work in
conjunction with the �KUKA Sunrise.Connectivity FastRobotInterface� toolkit.
In the following, the term �software� refers to all material directly
belonging to the provided SDK �Software development kit�, particularly source
code, libraries, binaries, manuals and technical documentation.

COPYRIGHT

All Rights Reserved
Copyright (C)  2014-2017 
KUKA Roboter GmbH
Augsburg, Germany

LICENSE 

Redistribution and use of the software in source and binary forms, with or
without modification, are permitted provided that the following conditions are
met:
a) The software is used in conjunction with KUKA products only. 
b) Redistributions of source code must retain the above copyright notice, this
list of conditions and the disclaimer.
c) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the disclaimer in the documentation and/or other
materials provided with the distribution. Altered source code of the
redistribution must be made available upon request with the distribution.
d) Modification and contributions to the original software provided by KUKA
must be clearly marked and the authorship must be stated.
e) Neither the name of KUKA nor the trademarks owned by KUKA may be used to
endorse or promote products derived from this software without specific prior
written permission.

DISCLAIMER OF WARRANTY

The Software is provided "AS IS" and "WITH ALL FAULTS," without warranty of
any kind, including without limitation the warranties of merchantability,
fitness for a particular purpose and non-infringement. 
KUKA makes no warranty that the Software is free of defects or is suitable for
any particular purpose. In no event shall KUKA be responsible for loss or
damages arising from the installation or use of the Software, including but
not limited to any indirect, punitive, special, incidental or consequential
damages of any character including, without limitation, damages for loss of
goodwill, work stoppage, computer failure or malfunction, or any and all other
commercial damages or losses. 
The entire risk to the quality and performance of the Software is not borne by
KUKA. Should the Software prove defective, KUKA is not liable for the entire
cost of any service and repair.



\file
\version {1.14}
*/
#include <cstring>
#include <cstdio>
#include "LBRJointOverlayClient.h"
#include "friLBRState.h"
// Visual studio needs extra define to use math constants
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

using namespace KUKA::FRI;
//******************************************************************************
LBRJointOverlayClient::LBRJointOverlayClient(){

   //this->targetDriveAngles = std::vector<double>(7);
	this->targetDriveAngles = new double[NUM_JOINTS];
	this->measuredDriveAngles = new double[NUM_JOINTS];
	this->getMeasJntPos = new double[NUM_JOINTS];
	this->isInCommandMode = false;
	this->areDriveAnglesSet = false;

	measuredJntAngles = std::vector<double>(NUM_JOINTS);

	commandModeMutex = new std::mutex;
	updateTrgAnglesMutex = new std::mutex;
	driveAnglesSetMutex = new std::mutex;
	currentDriveAnglesMutex = new std::mutex;

	this->sampleTime = 0;
}

//******************************************************************************
LBRJointOverlayClient::~LBRJointOverlayClient()
{
	this->isInCommandMode = false;
}
      
//******************************************************************************
void LBRJointOverlayClient::onStateChange(ESessionState oldState, ESessionState newState)
{
   LBRClient::onStateChange(oldState, newState);
   // (re)initialize sine parameters when entering Monitoring
   switch (newState)
   {
      case MONITORING_WAIT:
      case MONITORING_READY:
      {
		 this->commandModeMutex->lock();
		 this->isInCommandMode = false;
		 this->commandModeMutex->unlock();
		 this->sampleTime = robotState().getSampleTime();
         break;
      }
	  case COMMANDING_WAIT:
	  case COMMANDING_ACTIVE:
		  this->commandModeMutex->lock();
		  this->isInCommandMode = true;
		  this->commandModeMutex->unlock();
		  break;
      default:
      {
		  this->commandModeMutex->lock();
		  this->isInCommandMode = false;
		  this->commandModeMutex->unlock();
		  std::cout << "broke out" << std::endl;
         break;
      }
   }
}
   
//******************************************************************************
void LBRJointOverlayClient::command()
{



	this->driveAnglesSetMutex->lock();
	this->innerAreDriveAnglesSet = this->areDriveAnglesSet;
	this->driveAnglesSetMutex->unlock();

	currentDriveAnglesMutex->lock();
	memcpy(measuredDriveAngles, robotState().getMeasuredJointPosition(), LBRState::NUMBER_OF_JOINTS * sizeof(double));
	this->areMeasuredDriveAnglesEstablished = true;
	this->newDriveAngs = true;
	currentDriveAnglesMutex->unlock();


	double jointPos[LBRState::NUMBER_OF_JOINTS];
	memcpy(jointPos, robotState().getIpoJointPosition(), LBRState::NUMBER_OF_JOINTS * sizeof(double));
	
	double targetPos[LBRState::NUMBER_OF_JOINTS];
	this->updateTrgAnglesMutex->lock();
	memcpy(targetPos, targetDriveAngles, LBRState::NUMBER_OF_JOINTS * sizeof(double));
	this->updateTrgAnglesMutex->unlock();
	//robotCommand().setJointPosition(jointPos);

	this->commandModeMutex->lock();
	//this->innerIsInCommandMode = this->isInCommandMode;
	if (this->isInCommandMode && innerAreDriveAnglesSet) {
		//commanding when drive angles have been set an is in command mode
		robotCommand().setJointPosition(targetPos);
		//robotCommand().setJointPosition(jointPos);
	}
	else{
		//if in command mode but drive angles are not set keep drive angles where they are at.
		//if (this->innerIsInCommandMode) {
			robotCommand().setJointPosition(jointPos);
		//}
    }
	this->commandModeMutex->unlock();

}

int LBRJointOverlayClient::updateAngles(std::vector<double> angles)
{
	this->driveAnglesSetMutex->lock();
	this->areDriveAnglesSet = true;
	this->driveAnglesSetMutex->unlock();

	this->updateTrgAnglesMutex->lock();
	for (int i = 0; i < NUM_JOINTS; i++) {
		targetDriveAngles[i] = angles.at(i);
	}
	this->updateTrgAnglesMutex->unlock();

	return 0;
}
bool LBRJointOverlayClient::getIsInCommandMode(void)
{
	bool tempIsInCommandMode;

	this->commandModeMutex->lock();
	tempIsInCommandMode = this->isInCommandMode;
	this->commandModeMutex->unlock();

	return tempIsInCommandMode;
}

std::vector<double> LBRJointOverlayClient::getMeasuredJointPosition(bool * wasUpdated)
{
	currentDriveAnglesMutex->lock();
	bool areDriveAnglesEstablished = this->areMeasuredDriveAnglesEstablished;
	currentDriveAnglesMutex->unlock();
	while (!areDriveAnglesEstablished) {
		currentDriveAnglesMutex->lock();
		bool areDriveAnglesEstablished = this->areMeasuredDriveAnglesEstablished;
		currentDriveAnglesMutex->unlock();
		boost::this_thread::sleep_for(boost::chrono::microseconds(500));
		std::cout << "in wait for areMeasuredDriveAngEst" << std::endl;
	}
	if (newDriveAngs) {
		currentDriveAnglesMutex->lock();
		//memcpy(this->getMeasJntPos, measuredDriveAngles, LBRState::NUMBER_OF_JOINTS * sizeof(double));
		for (int i = 0; i < LBRState::NUMBER_OF_JOINTS; i++) {
			measuredJntAngles.at(i) = measuredDriveAngles[i];
		}
		this->newDriveAngs = false;
		currentDriveAnglesMutex->unlock();
		*wasUpdated = true;
	}
	else {
		*wasUpdated = false;
	}
	return measuredJntAngles;
}

//******************************************************************************
// clean up additional defines
#ifdef _USE_MATH_DEFINES
#undef _USE_MATH_DEFINES
#endif
