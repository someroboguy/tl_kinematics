#include <LBRJointOverlayClient.h>
#include <friClientApplication.h>
#include <friUdpConnection.h>
#include <boost/thread/thread.hpp>
#include <iostream>

#ifndef FRI_CLIENT_H
#define FRI_CLIENT_H

#define NUM_JOINTS 7

#define DEFAULT_PORTID 30200;


class FRIClient {
public:

	FRIClient();
	bool connect(void);

	bool isConnected(void);
	bool isFRIInActiveMode(void);
	bool updateAngles(std::vector<double> angles);
	double getSecSampleTime(void);
	std::vector<double> getMeasuredJointAngs(bool * wasUpdated);
	std::vector<double> getMeasuredJointAngs(void);
	

private:

	LBRJointOverlayClient * trafoClient;
	KUKA::FRI::UdpConnection * connection;
	KUKA::FRI::ClientApplication * app;
	char* hostname;
	int port;
	bool isFRIClientConnected;
	std::thread FRIClientThread_;
	std::thread FRISmartMonitorThread_;

	std::mutex * killTSetMutex;
	bool killT = false;

	bool internalConnectFRIClient_(void);
};


#endif // !FRI_CLIENT_H