#pragma once
//CPP libaries
//#include <windows.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

//Included libraries
#include <boost/filesystem.hpp>
#include <pcl/common/transforms.h>
#include <pcl/common/common_headers.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>

#include <pcl/io/vtk_lib_io.h>

static const int unusedFileIO = 0;

//Local Libraries
#include "iiwaRobot.h"



class fileIO
{
public:
	//TODO: figure out how to template without having to make inline (ie how not in header)

	//Loads a pcd file into the given cloud, uses default pointcloud path if filename does not have a path
	template <typename pointT>
	static bool loadFromPCD(typename pcl::PointCloud<pointT>::Ptr &cloud, std::string filename)
	{
		if (cloud->points.size() > 0) { cloud->points.clear(); }
		std::string fullPath = filename;
		if (filename.find("/") == std::string::npos && filename.find("\\") == std::string::npos) //checks to see if you provided the full path
		{
			fullPath = pointCloudDir; //if you did not provide a full path it gives the path to the PointClouds folder
			fullPath.append(filename);
		}
		std::cout << "load path: " << fullPath << std::endl;
		pcl::PCDReader pcdInput = pcl::PCDReader();
		try
		{
			pcdInput.read(fullPath, *cloud);
			return true;
		}
		catch (...)
		{
			return false;
		}
	}

	//Saves the cloud to file, uses default pointcloud path if filename does not have a path
	template <typename pointT>
	static bool saveToPCD(typename pcl::PointCloud<pointT>::Ptr cloud, std::string filename, bool binaryFlag = true)
	{
		if (cloud->points.size() == 0)
		{
			std::cout << "Cloud empty, save aborted!" << std::endl;
			return false;
		}
		std::string fullPath = filename;
		if (filename.find("/") == std::string::npos && filename.find("\\") == std::string::npos)
		{
			fullPath = pointCloudDir;
			fullPath.append(filename);
		}
		std::cout << "save path: " << fullPath << std::endl;
		pcl::PCDWriter pcdOutput = pcl::PCDWriter();
		if (binaryFlag)
		{
			pcdOutput.writeBinary<pointT>(fullPath, *cloud);
		}
		else
		{
			cloud->width = cloud->points.size();
			cloud->height = 1;
			pcdOutput.writeASCII<pointT>(fullPath, *cloud);
		}
		return true;
	}

	static bool saveSTL(pcl::PolygonMesh mesh, std::string filename);
	static bool loadSTL(pcl::PolygonMesh &mesh, std::string filename);

	static void saveRobotPose(Robot currentRobot, std::string fileName);
	static Robot loadRobotPose(std::string fileName = "");

	static void saveRobotConfig(std::vector<Eigen::Matrix4f> nullOriFrames);
	static std::vector<Eigen::Matrix4f> loadRobotConfig();


	static std::vector<pcl::PolygonMesh> loadRobotMesh(bool highRes = true);


private:
	static bool dirExists(const std::string& dirName_in);
	static std::string adjunctResourcesDir;
	static std::string essentialResourcesDir;

	static bool localDirSet;
	static void updateLocalDir()
	{
		localDirSet = true;
		int breakoutCounter = 0;
		while(!dirExists(essentialResourcesDir))
		{
			breakoutCounter++;
			adjunctResourcesDir = "../" + adjunctResourcesDir;
			essentialResourcesDir = "../" + essentialResourcesDir;
			if (breakoutCounter > 5) { std::cout << "Failed to locate local directories, all local file operations will fail..." << std::endl; return; }
		}
	}

	static std::vector<std::string> splitStr(std::string msg, char delimiter)
	{
		std::vector<std::string> result;
		const char *str = msg.c_str();
		char c = delimiter;
		do
		{
			const char *begin = str;
			while (*str != c && *str)
				str++;

			result.push_back(std::string(begin, str));
		} while (0 != *str++);
		return result;
	}

};

