#include "fileIO.h"

//initialize paths. TODO: figure out how to expose this or make it not hardcoded
std::string fileIO::adjunctResourcesDir = "../../adjunctResources";
std::string fileIO::essentialResourcesDir = "../../essentialResources";
bool fileIO::localDirSet = false;

bool fileIO::saveSTL(pcl::PolygonMesh mesh, std::string filename)
{
	if (!localDirSet) { updateLocalDir(); }
	std::string fullPath = filename;
	if (filename.find("/") == std::string::npos && filename.find("\\") == std::string::npos)
	{
		fullPath = adjunctResourcesDir + "/STLs";
		fullPath.append(filename);
	}
	if(dirExists(fullPath))

	std::cout << "save path: " << fullPath << std::endl;
	pcl::io::savePolygonFileSTL(fullPath, mesh);
	return true;
}

bool fileIO::loadSTL(pcl::PolygonMesh &mesh, std::string filename)
{
	if (!localDirSet) { updateLocalDir(); }
	std::string fullPath = filename;
	if (filename.find("/") == std::string::npos && filename.find("\\") == std::string::npos)
	{
		fullPath = adjunctResourcesDir + "/STLs";
		fullPath.append(filename);
	}
	std::cout << "load path: " << fullPath << std::endl;

	pcl::io::loadPolygonFileSTL(fullPath, mesh);
	return true;
}


void fileIO::saveRobotPose(Robot currentRobot, std::string fileName)
{
	if (!localDirSet) { updateLocalDir(); }
	
	std::string fullPath = essentialResourcesDir + "/robotConfiguration/JointFrameSnapshots/" + fileName;
	ofstream outfile(fullPath, ios::out);
	if (outfile.is_open()) { std::cout << "file opened" << std::endl; }
	else { std::cout << "file open failed" << std::endl; return; }
	for (int i = 0; i < currentRobot.getJointCount(); i++)
	{
		outfile << currentRobot.getJointAngle(i) << ",";
		outfile << std::endl;
	}
	outfile.close();
}

Robot fileIO::loadRobotPose(std::string fileName)
{
	if (!localDirSet) { updateLocalDir(); }
	Robot outRob = Robot(loadRobotConfig());
	if (fileName == "") { return outRob; }

	std::string fullPath = essentialResourcesDir + "/robotConfiguration/JointFrameSnapshots/" + fileName;
	ifstream inFile(fullPath, ios::out);
	if (inFile.is_open()) { std::cout << "file opened" << std::endl; }
	else { std::cout << "file open failed" << std::endl; return outRob; }
	
	

	std::vector<double> tempJointAngles = std::vector<double>();

	std::string line;
	if (std::getline(inFile, line))
	{
		std::vector<std::string> toks = splitStr(line, ',');
		for (int i = 0; i < outRob.getJointCount(); i++)
		{
			tempJointAngles.push_back(std::stod(toks[i]));
		}
		outRob.gotoAngles(tempJointAngles);
	}
	return outRob;

}

void fileIO::saveRobotConfig(std::vector<Eigen::Matrix4f> nullOriFrames)
{
	if (!localDirSet) { updateLocalDir(); }

	std::string fullPath = essentialResourcesDir + "/robotConfig/configV1.txt";
	ofstream outfile(fullPath, ios::out);
	if (outfile.is_open()) { std::cout << "file opened" << std::endl; }
	else { std::cout << "file open failed" << std::endl; return; }
	for (int i = 0; i < nullOriFrames.size(); i++)
	{
		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				outfile << nullOriFrames[i](x, y) << ",";
			}
		}

		outfile << std::endl;
	}
	outfile.close();
}

std::vector<Eigen::Matrix4f> fileIO::loadRobotConfig()
{
	if (!localDirSet) { updateLocalDir(); }

	std::vector<Eigen::Matrix4f> outVec = std::vector<Eigen::Matrix4f>();

	std::string fullPath = essentialResourcesDir + "/robotConfig/configV1.txt";
	ifstream inFile(fullPath);
	if (inFile.is_open()) { std::cout << "file opened" << std::endl; }
	else { std::cout << "file open failed" << std::endl; return outVec; }

	std::string line;
	while (std::getline(inFile, line))
	{
		std::vector<std::string> toks = splitStr(line, ',');
		Eigen::Matrix4f tempFrame = Eigen::Matrix4f::Identity();
		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				tempFrame(x, y) = std::stod(toks[4 * x + y]);
			}
		}
		outVec.push_back(tempFrame);
	}
	return outVec;
}

std::vector<pcl::PolygonMesh> fileIO::loadRobotMesh(bool highRes)
{
	if (!localDirSet) { updateLocalDir(); }

	std::string fullPath = essentialResourcesDir + "/robotSTL/";
	if (highRes) { fullPath += "NullOriSTL_HighRes/"; }
	else { fullPath += "NullOriSTL_LowRes/"; }

	std::vector<pcl::PolygonMesh> roboMeshes = std::vector<pcl::PolygonMesh>();
	for (int i = 0; i < 8; i++)
	{
		pcl::PolygonMesh tempMesh;
		loadSTL(tempMesh, fullPath + "J" + std::to_string(i) + "_NullOri.stl");
		roboMeshes.push_back(tempMesh);
	}
	return roboMeshes;
}








/////////////Privates

bool fileIO::dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}
