#include "visualizer.h"

std::vector<pcl::PolygonMesh> RoboVis::nullPosRobotMesh = std::vector<pcl::PolygonMesh>();

boost::shared_ptr<pcl::visualization::PCLVisualizer> RoboVis::spawnRoboViewer()
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("Robot Viewer"));
	viewer->setBackgroundColor(0, 0, 0);
	viewer->addCoordinateSystem(100);
	viewer->initCameraParameters();

	//TODO: make camera actually position rather than the current garbage of having to zoom every time
	//viewer->resetCamera();
	viewer->setCameraClipDistances(0, 5000);
	viewer->setCameraPosition(500, 1500, 750, 0, -1, 0);
	//viewer->updateCamera();
	return viewer;
}

void RoboVis::drawRobotShell(boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, Robot currentPose, bool highRes)
{
	int jointCount = currentPose.getJointCount();
	if (nullPosRobotMesh.size() < jointCount)
	{
		nullPosRobotMesh = fileIO::loadRobotMesh(highRes);
	}
	for (int i = 0; i < jointCount; i++)
	{
		std::string meshName = "robotMesh_J" + std::to_string(i);
		if (!visualizer->contains(meshName))
		{
			visualizer->addPolygonMesh(nullPosRobotMesh[i], meshName);
		}
		if (i > 0)
		{
			visualizer->updatePointCloudPose(meshName, Eigen::Affine3f(currentPose.getJointFrame(i-1)));
		}
		
	}
}

void RoboVis::drawRobotFrames(boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, Robot currentPose)
{
	int jointCount = currentPose.getJointCount();
	for (int i = 0; i < jointCount; i++)
	{
		std::string objName = "robotJointFrame_" + std::to_string(i);
		drawRefFrame(currentPose.getJointFrame(i), objName, visualizer, 100);
	}
}

void RoboVis::drawRobotPath(boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, std::vector<Eigen::Matrix4f> path)
{
	for (int i = 0; i < path.size(); i++)
	{
		std::string objName = "pathFrame" + std::to_string(i);
		drawRefFrame(path[i], objName, visualizer,1);
	}
}

void RoboVis::drawDebugPoints(boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{

	for (int i = 0; i < cloud->points.size(); i++)
	{
		std::string name = "debugSphere_"+std::to_string(i);
		if (!visualizer->contains(name))
		{
			visualizer->addSphere(pcl::PointXYZ(),25,name);
			visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0.5, 0, 0, name);
			//visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, name);
		}
		Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();
		pose(0, 3) = cloud->points[i].x;
		pose(1, 3) = cloud->points[i].y;
		pose(2, 3) = cloud->points[i].z;
		visualizer->updateShapePose(name, Eigen::Affine3f(pose));
	}
}


void RoboVis::drawRefFrame(Eigen::Matrix4f frame, std::string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, float sizeMM, float angle)
{
	pcl::ModelCoefficients xAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients yAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients zAxis = pcl::ModelCoefficients();

	//x,y,z,dx,dy,dz,angle
	xAxis.values = std::vector<float>{ 0, 0, 0, sizeMM, 0, 0, angle };
	yAxis.values = std::vector<float>{ 0, 0, 0, 0, sizeMM, 0, angle };
	zAxis.values = std::vector<float>{ 0, 0, 0, 0, 0, sizeMM, angle };

	std::string xName = name + "_x";
	if (!visualizer->contains(xName))
	{
		visualizer->addCone(xAxis, xName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0.5, 0, 0, xName);
		//visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, xName);
	}
	visualizer->updateShapePose(xName, Eigen::Affine3f(frame));

	std::string yName = name + "_y";
	if (!visualizer->contains(yName))
	{
		visualizer->addCone(yAxis, yName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0.5, 0, yName);
		//visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, yName);
	}
	visualizer->updateShapePose(yName, Eigen::Affine3f(frame));

	std::string zName = name + "_z";
	if (!visualizer->contains(zName))
	{
		visualizer->addCone(zAxis, zName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 0.5, zName);
		//visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, zName);
	}
	visualizer->updateShapePose(zName, Eigen::Affine3f(frame));
}

void RoboVis::drawVectorCone(pcl::PointNormal ptN, std::string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, float sizeMM)
{
	//x,y,z,dx,dy,dz,angle
	pcl::ModelCoefficients zAxis = pcl::ModelCoefficients();
	zAxis.values = std::vector<float>{ ptN.x, ptN.y, ptN.z, sizeMM * ptN.normal_x, sizeMM * ptN.normal_y, sizeMM * ptN.normal_z, 10 };
	std::string zName = name + "_z";
	if (!visualizer->contains(zName))
	{
		visualizer->addCone(zAxis, zName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 0.5, zName);
		//visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, zName);
	}
	//TODO:need to fix this to do something if it is already there for dynamic tracking visualizers
	//visualizer->updateShapePose(zName, Eigen::Affine3f(frame));
}

void RoboVis::meshVisualizer(pcl::PolygonMesh  objMesh)
{
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
	viewer->setBackgroundColor(0, 0, 0);
	viewer->addPolygonMesh(objMesh);
	viewer->addCoordinateSystem(100);
	viewer->initCameraParameters();
	while (!viewer->wasStopped())
	{
		viewer->spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}
}

void RoboVis::fixturedMeshViewer(std::vector<pcl::PolygonMesh> meshes, std::vector<Eigen::Matrix4f> handles)
{
	if (meshes.size() != handles.size()) { std::cout << "Handles do not match meshes"; }

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
	viewer->setBackgroundColor(0, 0, 0);

	std::vector<Eigen::Matrix4f> visHandles = std::vector<Eigen::Matrix4f>();
	for (int i = 0; i < meshes.size(); i++)
	{
		viewer->addPolygonMesh(meshes[i], "mesh"+std::to_string(i));
		drawRefFrame(handles[i], "handle" + std::to_string(i), viewer, 20);
		visHandles.push_back(Eigen::Matrix4f::Identity());
	}

	viewer->addCoordinateSystem(100);
	viewer->initCameraParameters();

	double angle = 0;
	double angleStep = M_PI / 320;
	double angleLim = M_PI / 8;
	while (!viewer->wasStopped())
	{
		viewer->spinOnce(50);
		
		angle += angleStep;
		if (abs(angle) >= angleLim) { angleStep = -angleStep; }

		for (int i = 0; i < meshes.size(); i++)
		{
			for (int ii = i + 1; ii < meshes.size(); ii++)
			{
				rotateAny(
					visHandles[ii],
					Eigen::Vector3f(handles[i](0, 3), handles[i](1, 3), handles[i](2, 3)),
					Eigen::Vector3f(handles[i](0, 2), handles[i](1, 2), handles[i](2, 2)),
					angleStep);
				rotateAny(
					handles[ii],
					Eigen::Vector3f(handles[i](0, 3), handles[i](1, 3), handles[i](2, 3)),
					Eigen::Vector3f(handles[i](0, 2), handles[i](1, 2), handles[i](2, 2)),
					angleStep);
				viewer->updatePointCloudPose("mesh" + std::to_string(ii), Eigen::Affine3f(visHandles[ii]));
				drawRefFrame(handles[ii], "handle" + std::to_string(ii), viewer, 20);
			}
		}
	}
}

Eigen::Matrix4f RoboVis::rotateAny(Eigen::Matrix4f &inFrame, Eigen::RowVector3f pt, Eigen::RowVector3f dir, double angle)
{
	//https://sites.google.com/site/glennmurray/Home/rotation-matrices-and-formulas/rotation-about-an-arbitrary-axis-in-3-dimensions
	Eigen::Matrix4f transformation = Eigen::Matrix4f::Identity();

	dir.normalize();

	//pt: a,b,c
	//dir: u,v,w
	float a = pt(0);
	float b = pt(1);
	float c = pt(2);

	float u = dir(0);
	float v = dir(1);
	float w = dir(2);

	transformation(0, 0) = u * u + (v * v + w * w) * cos(angle);
	transformation(0, 1) = u * v * (1.0 - cos(angle)) - w * sin(angle);
	transformation(0, 2) = u * w * (1.0 - cos(angle)) + v * sin(angle);
	transformation(0, 3) = (a * (v * v + w * w) - u * (b * v + c * w)) * (1.0 - cos(angle)) + (b * w - c * v) * sin(angle);

	transformation(1, 0) = u * v * (1.0 - cos(angle)) + w * sin(angle);
	transformation(1, 1) = v * v + (u * u + w * w) * cos(angle);
	transformation(1, 2) = v * w * (1.0 - cos(angle)) - u * sin(angle);
	transformation(1, 3) = (b * (u * u + w * w) - v * (a * u + c * w)) * (1.0 - cos(angle)) + (c * u - a * w) * sin(angle);

	transformation(2, 0) = u * w * (1.0 - cos(angle)) - v * sin(angle);
	transformation(2, 1) = v * w * (1.0 - cos(angle)) + u * sin(angle);
	transformation(2, 2) = w * w + (u * u + v * v) * cos(angle);
	transformation(2, 3) = (c * (u * u + v * v) - w * (a * u + b * v)) * (1.0 - cos(angle)) + (a * v - b * u) * sin(angle);

	transformation(3, 0) = 0;
	transformation(3, 1) = 0;
	transformation(3, 2) = 0;
	transformation(3, 3) = 1.0;

	inFrame = transformation * inFrame;
	return transformation;
}



