#pragma once
//CPP libaries
#include <iostream>

//Included libraries
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/features/principal_curvatures.h>


static const int unusedVisualizer = 0;
//Local Libraries
#include "iiwaRobot.h"
#include "fileIO.h"

class RoboVis
{
public:

	static void meshVisualizer(pcl::PolygonMesh objMesh);

	static void fixturedMeshViewer(std::vector<pcl::PolygonMesh> meshes, std::vector<Eigen::Matrix4f> handles);

	static boost::shared_ptr<pcl::visualization::PCLVisualizer> spawnRoboViewer();

	static void drawRobotPath(boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, std::vector<Eigen::Matrix4f> path);
	static void drawRobotShell(boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, Robot currentPose, bool highRes = true);
	static void drawRobotFrames(boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, Robot currentPose);
	static void drawDebugPoints(boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
	static void drawRefFrame(Eigen::Matrix4f frame, std::string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, float sizeMM = 10, float angle = 10);


private:
	


	static void drawVectorCone(pcl::PointNormal ptN, std::string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> &visualizer, float sizeMM = 10);

	static Eigen::Matrix4f rotateAny(Eigen::Matrix4f &inFrame, Eigen::RowVector3f pt, Eigen::RowVector3f dir, double angle);//TODO: update this

	

	static std::vector<pcl::PolygonMesh> nullPosRobotMesh;
};